# [ML] Lesson4

These are the Jupyter Notebooks developed to perform the [fifth](http://home.agh.edu.pl/~mdig/dokuwiki/doku.php?id=teaching:courses:agh:weaiiib:inf:adv-ml:2018-19_l:labs:lab09) and the [sixth](http://home.agh.edu.pl/~mdig/dokuwiki/doku.php?id=teaching:courses:agh:weaiiib:inf:adv-ml:2018-19_l:labs:lab13) assignment.